# Banquo

Banquo is UI automation for MyResearch to test, for now, some simple assertions, e.g. does the page for eGC1 A107191 contain the text "A107191."

[Banquo](https://en.wikipedia.org/wiki/Banquo) was a ghost in _Macbeth_. Why a ghost? Because this app uses [Nightmare](https://github.com/segmentio/nightmare) which uses [PhantomJS](http://phantomjs.org/) and phantoms are... ghosts!
